import * as links from './modules/link-events.js';
import * as filter from './modules/filter-events.js';

window.onload = () => {
    links.addClickListeners();
    filter.addClickListeners();

    const spoilers = document.querySelectorAll('.spoiler');
    spoilers.forEach(elem => elem.addEventListener('toggle', () => {
            const spoilerBody = elem.querySelector('.spoiler__body');
            spoilerBody.style.animationPlayState = "running";
            spoilerBody.addEventListener('animationend', () =>
                spoilerBody.style.animationPlayState = "paused"
            );
        })
    );
}



