export function addClickListeners() {
    filterButtons.forEach((elem) => {
        elem.addEventListener('click', chooseTopicFilter);
    });

    prevButton.addEventListener('click', () => switchLectures("prev"));
    nextButton.addEventListener('click', () => switchLectures("next"));
}


const allLectures = document.querySelectorAll('.lecture'); // все лекции страницы
const allLecturesCourse = document.querySelector('.course-level_all'); // блок со всеми лекциями

const filterTopics = new Set(); // множество всех тем лекций
const topicLectures = {}; // объект, хранящий всю информацию о лекциях

// Кнопки слайдеров, листающих лекции в курсах
const prevButton = document.querySelector('.course-level__prev-button');
const nextButton = document.querySelector('.course-level__next-button');

const lecturesInCourseContent = 4; // количество карточек лекций в блоке курса

let index = 0; // индекс для работы слайдера с объектом лекций
let activeFilterButton; // активный фильтр лекций по темам
// Место контента блока для заполнения лекциями
let courseContent = allLecturesCourse.querySelector('.course-level__content');


// main
addFilterButtons();
addAllLectures();
const filterButtons = document.querySelectorAll('.course-level__filter-button');


// Выделить все темы в filterTopics и заполнить объект topicLectures
function fillTopicLectures() {
    allLectures.forEach(value => {
        const topics = value.getAttribute('filter-topic').split(' ');
        const newLecture = {
            title: value.querySelector('.lecture__title').innerHTML, // Заголовок лекции
            description: value.querySelector('.lecture__subtitle').innerHTML, // Описание лекции
            date: value.querySelector('.lecture__date').getAttribute('datetime'), // Дата лекции
            image: value.querySelector('.lecture__background').getAttribute('src'), // Image лекции
            label: value.querySelector('.lecture__caption').innerHTML, // Лейбл лекции
            html: value.cloneNode(true), // Копия html-кода лекции
        }
        topics.forEach(topic => {
            if (!filterTopics.has(topic)) {
                filterTopics.add(topic);
                topicLectures[topic] = [];
            }
            topicLectures[topic].push(newLecture);
        });
        topicLectures.all.push(newLecture);
    });
}

// Добавить кнопки-фильтры по отдельным темам
function addFilterButtons() {
    filterTopics.forEach(value => {
        const template = document.getElementById('new-filter-button').content.cloneNode(true);
        const button = template.querySelector('.button');

        button.setAttribute('filter-topic', value);
        button.innerText = value;

        allLecturesCourse.querySelector('.course-level__navbar').prepend(template);
    });
}

// Добавить лекции в блок "все лекции"
function addAllLectures() {
    // Добавить все лекции в темы и в массив лекций по темам
    filterTopics.add("all");
    topicLectures.all = [];

    fillTopicLectures();
    addFilterButtons();

    // Добавить количество лекций в кнопку-фильтр и сделать её активной
    activeFilterButton = allLecturesCourse.querySelector('.button[filter-topic="all"]');
    activeFilterButton.innerHTML = allLectures.length + ' лекций';
    activeFilterButton.classList.add('active');
    switchLectures();
}

// Обработчик нажатия по фильтру
function chooseTopicFilter() {
    index = 0;
    activeFilterButton.classList.remove('active');
    this.classList.add('active');
    activeFilterButton = this;

    switchLectures();
}

// Переключение лекций по нажатию кнопок слайдера
function switchLectures(direction) {
    const topic = activeFilterButton.getAttribute('filter-topic');
    const lectures = topicLectures[topic];

    courseContent.innerHTML = '';

    if (direction === "next") {
        index += lecturesInCourseContent;
    } else if (direction === "prev") {
        index -= lecturesInCourseContent;
    }

    fixIndex(lectures.length);

    for (let i = index; i < index + lecturesInCourseContent; i++) {
        if (lectures[i]) {
            courseContent.append(lectures[i].html);
        } else {
            let template = document.getElementById('new-lecture-card').content.cloneNode(true);
            courseContent.append(template);
        }
    }
}

// Обработка случаев выхода индекса за допустимые пределы
function fixIndex(lecturesNumber) {
    if (index >= lecturesNumber) {
        index = 0;
    } else if (index < 0) {
        index = lecturesNumber - lecturesNumber % lecturesInCourseContent;
        index = index === lecturesNumber ? index - lecturesInCourseContent : index;
    }
}
