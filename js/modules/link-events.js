const sidebarLinks = document.querySelectorAll('.navbar__link_sidebar:not(.spoiler__header), .navbar__link-2');
const spoilerHeaders = document.querySelectorAll('.navbar__link_sidebar.spoiler__header');
let activeLink = document.querySelector('.navbar__link_sidebar:not(.spoiler__header).active, .navbar__link-2.active');

export function addClickListeners() {
    sidebarLinks.forEach((elem) => {
        elem.addEventListener('click', toggleActiveLink);
    });

    spoilerHeaders.forEach((elem) => {
        elem.addEventListener('click', hideOtherSpoilers);
    });
}

function toggleActiveLink() {
    if (this !== activeLink) {
        activeLink.classList.remove('active');
        toggleSpoilerActiveClass(activeLink);

        activeLink = this;
        activeLink.classList.add('active');
        toggleSpoilerActiveClass(activeLink);
    }
}

function hideOtherSpoilers() {
    spoilerHeaders.forEach((elem) => {
        if (elem !== this ) {
            elem.parentElement.removeAttribute('open');
            elem.nextElementSibling.style.animationPlayState = "paused";
        }
    });
}

function toggleSpoilerActiveClass(childLinkTag) {
    if (childLinkTag.classList.contains('navbar__link-2')) {
        childLinkTag.parentElement.parentElement.previousElementSibling.classList.toggle('active');
    }
}
